.phony: init all run clean tarball csc doc

TARGET=cppstreams
SOURCE_DIRECTORY=src
BUILD_DIRECTORY=build
BUILD_TYPE=DEBUG # or: RELEASE

# DO NOT MODIFY THE FILE BELOW THIS LINE (UNLESS YOU KNOW WHAT YOU ARE DOING)

init:
	mkdir -p $(BUILD_DIRECTORY) && ln -fs $(BUILD_DIRECTORY)/$(TARGET) $(TARGET) 

all: init
	cd $(BUILD_DIRECTORY) && cmake -DCMAKE_BUILD_TYPE:STRING=$(BUILD_TYPE)  .. && make

run: all
	 ./$(TARGET)

tarball: clean
	tar cvzf $(TARGET)_`date +"%Y%m%d_%H%M"`.tgz *
	
clean:
	rm -rf $(BUILD_DIRECTORY)
	rm -f $(TARGET)
	rm -rf README.html

csc:
	find $(SOURCE_DIRECTORY) -name "*.h" -o -name "*.cc" > cscope.files
	cscope -Rb

doc:
	lowdown -s -o README.html README.md
