# The cstreams library
## Introduction
The cstreams library provides means to convert containers into data streams and
apply operations (e.g. filtering, mapping and reduction) to the elements on the
fly. Streams can be created from any container (actually anything that has a
begin() and an end() method that returns an iterator with operator``*``,
operator++ and operator!=).

Small examples demonstrating the features of the library can be found in
``src/examples.cc``.

The example executable can be created using ``cmake`` or by issuing ``make
run`` (which also uses ``cmake`` so it has to be installed for compilation to work).

## Using cstreams
### Creating and iterating over streams
A stream can be created from a container using the ``to_stream()`` function. It
needs a container as an input and returns a stream that can be iterated over
(e.g. in a range-based ``for``):

```C++
namespace streams = h119::util::stream;

std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

for (auto i: streams::to_stream(v))
{
	std::cout << i << std::endl;
}
```

Streams also have collectors, so the output of a stream can be saved into another container:

```C++
namespace streams = h119::util::stream;

std::vector<int> v_in = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
std::vector<int> v_out = {-1};

streams::to_stream(v_in) >> streams::collect(std::back_inserter(v_out));
```

The ``std::back_inserter`` function is part of STL and it provides an
iterator-like construct the ``operator*`` of which adds the elements assigned
to the place (the position in a container) the iteratort points to. The example
above will simply copy ``v_in`` to the end of ``v_out`` (after the first and
only element).

Please note, that the ``operator >>`` was used to apply an operation on a
stream. This follows the C++ custom of creating and manipulating character
streams. All the other operations discussed later on use this operator, except
for those, that apply to streams rather than their elements.

### Operations on the elements of streams
Of course, the example above is not a very useful application of the library.
There are much easier and more effective ways to make a copy of a container.

#### Filtering streams
The first operation we'll discuss, is filtering. The elements of a stream can
be filtered using function-like object (e.g. function, method, functor, lambda
-- anything that a ``std::function`` can hold).

The following example will print the even numbers from a stream of ``int``s:

```C++
namespace streams = h119::util::stream;

std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

for
(
	double i:
		streams::to_stream(v)
			>> streams::filter<int>{[](int const &i){return i % 2 == 0;}}
)
{
	std::cout << i << std::endl;
}
```

Filters can also be chained: the following example will only print 10.

```C++
namespace streams = h119::util::stream;

std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

for
(
	double i:
		streams::to_stream(v)
			>> streams::filter<int>{[](int const &i){return i % 2 == 0;}}
			>> streams::filter<int>{[](int const &i){return i % 5 == 0;}}
)
{
	std::cout << i << std::endl;
}
```

#### Transforming streams
The elements of a stream can be altered: their value even their type can be
changed thus the stream is transformed or mapped to another stream.

The example filters the odd numbers from a stream of ``int``s and then divides
them by two and thus converts them to ``double``s.

```C++
namespace streams = h119::util::stream;

std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

for
(
	double i:
		streams::to_stream(v)
			>> streams::filter<int>{[](int const &i){return i % 2 == 1;}}
			>> streams::map<int, double>{[](int const &i) -> double {return 0.5 * i;}}
)
{
	std::cout << i << std::endl;
}
```

The ``h119::util::stream::map`` constructor expects two template parameters:
the first one is the element type of the input stream, the second one is that
of the output stream.

#### Reducing streams
Stream reduction is very similar to transformation but the basic idea here is
to reduce a stream of elements into one single value. The functor expected here
also has to template parameters: the type of the input stream and the type of
the output value.

The difference is that there are two function arguments as well: the first is
the intermediate value (that becomes the result after the last element had been
produced) and the current value. It has to produce a value based on the intermediate
result and the current value which will be stored in the variable holding the intermediate
value by the stream.

```C++
namespace streams = h119::util::stream;

std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

std::cout <<
	(
		streams::to_stream(v) >> streams::reduce<int, double>
		(
			1.0,
			[](double const &immediate, int const &current){ return immediate / (0.1 * current); }
		)
	)
<< std::endl;
```

#### Taking the first few elements of streams
The length of streams is sometimes unknown and there can also be infinite
streams (see later). Sometimes it is thus useful to be able to take the first
few elements of a stream: the ``first_n`` type of the library does just that.

It's constructor has one argument: the number of elements that should be taken
from the stream. If the stream is shorter than that, the output will be shorter
as well -- it will respect the length of its input stream and will not run over
the end.

```C++
namespace streams = h119::util::stream;

std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

for (auto i: streams::to_stream(v) >> streams::first_n{3})
{
	std::cout << i << std::endl;
}
```

The output will go from 1 to 3, of course. Later, we will see more useful
examples for this feature.

### Operations on streams
Operations can be performed not only on the elements of streams but on
streams themselves as well.

#### Concatenation
The most simple of stream operations is concatenation. It needs that the two
streams are of the same type and the output will be the same type as well.

The ``operator +`` is used to concatenate streams just as it is used to
concatenate STL strings. 

```C++
namespace streams = h119::util::stream;

std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
std::vector<int> v2 = {11, 22, 33, 44, 55, 66, 77, 88, 99, 110};

auto str1 = streams::to_stream(v1) >> streams::filter<int>{[](int const &i){return i % 2 == 0;}};
auto str2 = streams::to_stream(v2) >> streams::map<int, int>{[](int const &i){return 10 * i + 5;}};

for (auto p: str1 + str2)
{
	std::cout << p << std::endl;
}
```

#### Merging streams
Streams can be merged, which means that their corresponding elements will be
put in pairs: the first elements will become the first pair in the output
stream, the second elements become the second pair and so on.

```C++
namespace streams = h119::util::stream;

std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
std::vector<int> v2 = {11, 22, 33, 44, 55, 66, 77, 88, 99, 110};

auto str1 = streams::to_stream(v1);
auto str2 = streams::to_stream(v2);

for (auto p: str1 * str2)
{
	std::cout << p.first << " " << p.second << std::endl;
}
```

Please note that the ``operator *`` can be used to merge two streams.

All this is easy to do when the two streams are of exactly the same length. If,
on the other hand, one of them is longer than the other one, then we have a
problem: what value should go to the ``pair`` next to the elements of the rest
of the longer stream?

There is no correct answer: either the user of the library gives such a value
or the iteration simply shouldn't reach beyond the end of the shorter stream.
This is exactly what happens. If no default value is given for a stream (see a
bit later how that can be done) then the merged stream will end at the end of the
shorter one:

```C++
namespace streams = h119::util::stream;

std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
std::vector<int> v2 = {11, 22, 33};

auto str1 = streams::to_stream(v1);
auto str2 = streams::to_stream(v2);

for (auto p: str1 * str2)
{
	std::cout << p.first << " " << p.second << std::endl;
}
```

The output of the code fragment above is:

```
1 11
2 22
3 33
```

If the intended behaviour is to get the value-pairs until the end of the
shorter stream is reached and then a default value should be inserted next to
the elements of the longer stream: the ``operator |`` needs to be used with the
shorter stream. It make the shorter stream infinite: the beginning of the new
stream consists of the elements of the stream and they are followed by an
infinite stream containing the default value.

The ``operator |`` is a dangerous one as it can lead to an infinite loop. Of
course, if a stream created with this operator is merged with a finite stream
then there's no problem: the stream will end at the end of the finite stream
and the result is just what we wanted. The following code fragment demonstrates
this.

```C++
namespace streams = h119::util::stream;

std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
std::vector<int> v2 = {11, 22, 33};

auto str1 = streams::to_stream(v1);
auto str2 = streams::to_stream(v2);

for (auto p: str1 * (str2 | -1) )
{
	std::cout << p.first << " " << p.second << std::endl;
}
```

The output of the code fragment above is:

```
1 11
2 22
3 33
4 -1
5 -1
6 -1
7 -1
8 -1
9 -1
10 -1
```

Please note that it's not a good idea to give a default value to both operands
of a merge as it creates two infinite streams without a stop (this is where
``first_n`` described above comes handy).

#### Generating streams
Up to now, streams have always been made from containers but that's not the
only option. A function can be used to generate an infinite stream. It can be
used as a starting point for a complex calculation performed using streams.

Of course, an infinite stream is not very useful as it results in an infinite
loop if one tries to iterate over it, or fills the entire available memory if
one tries to collect it.

A solution for this problem can also be the ``first_n`` element of the
framework which simply takes the first _n_ elements of any stream (where _n_ is
a value given by the user).

```C++
namespace streams = h119::util::stream;

for (auto i: streams::generate([](std::size_t n){auto a = n + 1; return a * a;}) >> streams::first_n{10})
{
	std::cout << i << std::endl;
}
```

This example above prints the first 10 square numbers.

The ``generate`` construct needs to receive a functor that takes the index of the element to
generate and returns the generated element.

The merge operation together with ``generate`` can be used for example to
present the values of a stream together with their index:

```C++
namespace streams = h119::util::stream;

std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

auto str =
	streams::to_stream(v)
	>> streams::filter<int>([](int const &i){return i % 3;})
	>> streams::map<int, double>([](int const &i){return 3.2 * i + 5.0;});

for (auto p: streams::generate([](std::size_t number){return number + 1;}) * str)
{
	std::cout << p.first << ": " << p.second << std::endl;
}
```

##### Generating streams from files
It is possible to turn a ``fstream`` object into a ``h119::util::stream``. An
open and valid ``fstream`` object is needed for the entire lifetime of the
stream operations.

```C++
namespace streams = h119::util::stream;

std::fstream text{"README.md"};

for (auto c: streams::stream_file{text} >> streams::first_n{200})
{
	std::cout << c;
}

std::cout << std::endl;
```
