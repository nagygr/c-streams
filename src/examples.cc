#include <iostream>
#include <vector>
#include <functional>
#include <cstddef>
#include <iterator>
#include <string>

#include <h119/util/stream/streams.h>

using example_function = std::function< auto () -> void >;

auto simple_filters() -> void
{
	namespace streams = h119::util::stream;

	std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	for 
	(
		auto i:
			streams::to_stream(v)
				>> streams::filter<int>{[](int const &i){return i % 2 == 0;}}
				>> streams::filter<int>{[](int const &i){return i % 5 == 0;}}
	)
	{
		std::cout << i << std::endl;
	}
}

auto simple_map() -> void
{
	namespace streams = h119::util::stream;

	std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	for
	(
		double i:
			streams::to_stream(v)
				>> streams::filter<int>{[](int const &i){return i % 2 == 0;}}
				>> streams::map<int, double>{[](int const &i) -> double {return 2.0 * i;}}
	)
	{
		std::cout << i << std::endl;
	}
}

auto simple_merge() -> void
{
	namespace streams = h119::util::stream;

	std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	std::vector<int> v2 = {11, 22, 33, 44, 55, 66, 77, 88, 99, 110};

	auto str1 = streams::to_stream(v1) >> streams::filter<int>{[](int const &i){return i % 2 == 0;}};
	auto str2 = streams::to_stream(v2) >> streams::map<int, int>{[](int const &i){return 10 * i + 5;}};

	for (auto p: str1 * str2)
	{
		std::cout << p.first << " " << p.second << std::endl;
	}
}

auto merge_with_default() -> void
{
	namespace streams = h119::util::stream;

	std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	std::vector<int> v2 = {11, 22, 33, 44, 55, 66, 77, 88, 99, 110};

	auto str1 = streams::to_stream(v1) >> streams::filter<int>{[](int const &i){return i % 2 == 0;}};
	auto str2 = streams::to_stream(v2) >> streams::map<int, int>{[](int const &i){return 10 * i + 5;}};

	for (auto p: (str1 | -1) * str2)
	{
		std::cout << p.first << " " << p.second << std::endl;
	}
}

auto destination_back_inserter() -> void
{
	namespace streams = h119::util::stream;
	
	std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	std::vector<double> d = {-1.0};

	streams::to_stream(v)
	>> streams::filter<int>{[](int const &i){return i % 3 == 0;}}
	>> streams::map<int, double>{[](int const &i){return 2.3 * i;}}
	>> streams::collect(std::back_inserter(d));

	for (auto i: d)
	{
		std::cout << i << std::endl;
	}
}

auto collecting_merged_streams() -> void
{
	namespace streams = h119::util::stream;
	
	std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	std::vector<int> v2 = {2, 4, 6, 8};
	std::vector<std::pair<double, int>> d;

	auto str1 = 
		streams::to_stream(v1)
			>> streams::filter<int>{[](int const &i){return i % 3 == 0;}}
			>> streams::map<int, double>{[](int const &i){return 2.3 * i;}};

	auto str2 = streams::to_stream(v2) >> streams::map<int, int>([](int const &i){return 15 + 2 * i;});

	((str1 | -111) * str2) >> streams::collect(std::back_inserter(d));

	for (auto i: d)
	{
		std::cout << i.first << ", " << i.second << std::endl;
	}
}

auto collecting_transformed_streams() -> void
{
	namespace streams = h119::util::stream;
	
	std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	std::vector<int> v2 = {2, 4, 6, 8};
	std::vector<std::string> s;

	auto str1 = 
		streams::to_stream(v1)
			>> streams::filter<int>{[](int const &i){return i % 3 == 0;}}
			>> streams::map<int, double>{[](int const &i){return 2.3 * i;}};

	auto str2 = streams::to_stream(v2) >> streams::map<int, int>([](int const &i){return 15 + 2 * i;});

	((str1 | -111) * str2)
	>> streams::map<std::pair<double, int>, std::string>([](std::pair<double, int> const &p){return std::to_string(p.first) + " {=} " + std::to_string(p.second);})
	>> streams::collect(std::back_inserter(s));

	for (auto i: s)
	{
		std::cout << i << std::endl;
	}
}

auto simple_concatenate() -> void
{
	namespace streams = h119::util::stream;

	std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	std::vector<int> v2 = {11, 22, 33, 44, 55, 66, 77, 88, 99, 110};

	auto str1 = streams::to_stream(v1) >> streams::filter<int>{[](int const &i){return i % 2 == 0;}};
	auto str2 = streams::to_stream(v2) >> streams::map<int, int>{[](int const &i){return 10 * i + 5;}};

	for (auto p: str1 + str2)
	{
		std::cout << p << std::endl;
	}
}

auto simple_reducer() -> void
{
	namespace streams = h119::util::stream;

	std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	std::cout <<
		(
			streams::to_stream(v) >> streams::reduce<int, double>
			(
				1.0,
				[](double const &immediate, int const &current){ return immediate / (0.1 * current); }
			)
		)
	<< std::endl;
}

auto simple_generator() -> void
{
	namespace streams = h119::util::stream;

	std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	auto str = streams::to_stream(v) >> streams::filter<int>([](int const &i){return i % 3;}) >> streams::map<int, double>([](int const &i){return 3.2 * i + 5.0;});

	for (auto p: streams::generate([](std::size_t number){return number + 1;}) * str)
	{
		std::cout << p.first << ": " << p.second << std::endl;
	}
}

auto generator_with_first_n() -> void
{
	namespace streams = h119::util::stream;

	for (auto i: streams::generate([](std::size_t n){auto a = n + 1; return a * a;}) >> streams::first_n{10})
	{
		std::cout << i << std::endl;
	}
}

auto simple_file_stream() -> void
{
	namespace streams = h119::util::stream;

	std::fstream text{"README.md"};

	for (auto c: streams::stream_file{text} >> streams::first_n{200})
	{
		std::cout << c;
	}

	std::cout << std::endl;
}

auto main() -> int
{
	std::vector<example_function> examples =
	{
		simple_filters,
		simple_map,
		simple_merge,
		merge_with_default,
		destination_back_inserter,
		collecting_merged_streams,
		collecting_transformed_streams,
		simple_concatenate,
		simple_reducer,
		simple_generator,
		generator_with_first_n,
		simple_file_stream
	};

	std::size_t example_count = 0;
	for (auto example: examples)
	{
		std::cout << "*************************************************\n";
		std::cout << " EXAMPLE No. " << ++example_count << "\n";
		std::cout << "*************************************************\n";

		example();

		std::cout << "=================================================\n";
	}
}
