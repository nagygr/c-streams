#pragma once

#include <functional>
#include <iterator>
#include <utility>

namespace h119::util::stream
{

template <typename input_iterator_type, typename output_iterator_type>
struct stream_collector
{
	using from_value_type =
		typename std::remove_reference
		<
			decltype(**static_cast<input_iterator_type *>(nullptr))
		>::type;

	stream_collector
	(
		input_iterator_type from,
		input_iterator_type to,
		output_iterator_type destination
	)
	{
		for (; from != to; ++from) *destination = *from;
	}
};

template <typename iterable_type>
struct collect
{
	collect(iterable_type iterable): iterable_{iterable} {}

	auto iterable() const -> iterable_type 
	{
		return iterable_;
	}

	private:
		iterable_type iterable_;
};

template <typename iterable_source, typename iterable_destination>
auto operator >>
(
	iterable_source const &itr,
	iterable_destination const &dst
) -> stream_collector
<
	decltype(static_cast<iterable_source*>(nullptr)->begin()),
	typename std::remove_reference
	<
		decltype(static_cast<iterable_destination*>(nullptr)->iterable())
	>::type
>
{
	return stream_collector{itr.begin(), itr.end(), dst.iterable()};
}

}
