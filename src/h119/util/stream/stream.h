#pragma once

#include <vector>
#include <functional>
#include <iterator>

namespace h119::util::stream
{

template <typename iterator_type>
struct stream
{
	using value_type = decltype(**static_cast<iterator_type *>(nullptr));

	template <typename value_type>
	struct stream_iterator
	{
		stream_iterator(iterator_type current):
			current_{current}
		{}

		auto operator *() -> value_type const &
		{
			return *current_;
		}

		auto operator ++() -> stream_iterator<value_type> &
		{
			++current_;
			return *this;
		}

		auto operator !=(stream_iterator<value_type> const &other) const -> bool
		{
			return current_ != other.current_;
		}


		private:
			iterator_type current_;
	};

	stream(iterator_type from, iterator_type to):
		from_{from},
		to_{to}
	{}

	auto begin() const -> stream_iterator<value_type>
	{
		return stream_iterator<value_type>{from_};
	}

	auto end() const -> stream_iterator<value_type>
	{
		return stream_iterator<value_type>{to_};
	}

	private:
		iterator_type from_;
		iterator_type to_;
};

template <typename container>
auto to_stream(container &c) -> stream<decltype(std::begin(*static_cast<container *>(nullptr)))>
{
	return stream{std::begin(c), std::end(c)};
}

}
