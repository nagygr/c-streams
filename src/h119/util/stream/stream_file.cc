#include <cstddef>

#include <h119/util/stream/stream_file.h>

namespace h119::util::stream
{

stream_file::file_iterator::file_iterator
(
	h119::util::optional::optional_reference<std::fstream> file,
	std::streamsize buffer_size
):
	initialized_{false},
	file_{file},
	buffer_size_{buffer_size},
	current_{nullptr},
	end_of_buffer_{nullptr}
{
}

auto stream_file::file_iterator::fill_buffer() -> void
{
	if (!file_.has_value()) return;

	if (!buffer_)
		buffer_.reset(new char[static_cast<std::size_t>(buffer_size_)]);

	std::streamsize actual_buffer_size;

	actual_buffer_size = 
		file_->read(buffer_.get(), buffer_size_) ?
		buffer_size_ :
		file_->gcount();

	if (actual_buffer_size == 0)
	{
		current_ = nullptr;
		end_of_buffer_ = nullptr;
	}
	else
	{
		current_ = buffer_.get();
		end_of_buffer_ = buffer_.get() + actual_buffer_size;
	}

}

}
