#pragma once

#include <vector>
#include <functional>
#include <iterator>
#include <type_traits>

#include <h119/util/stream/stream.h>

namespace h119::util::stream
{

template <typename iterator_type>
struct stream_filter
{
	using value_type =
		typename std::remove_reference
		<
			decltype(**static_cast<iterator_type *>(nullptr))
		>::type;
	using predicate_type = std::function<auto ( value_type const & ) -> bool>;

	template <typename value_type>
	struct filter_iterator
	{
		filter_iterator(iterator_type current, iterator_type end, predicate_type predicate):
			current_{current},
			end_{end},
			predicate_{predicate}
		{
			find_next();
		}

		auto operator *() -> value_type &
		{
			return *current_;
		}

		auto operator ++() -> filter_iterator<value_type> &
		{
			++current_;
			find_next();
			return *this;
		}

		auto operator !=(filter_iterator<value_type> const &other) const -> bool
		{
			return current_ != other.current_;
		}


		private:
			iterator_type current_;
			iterator_type end_;
			predicate_type predicate_;

			auto find_next() -> void
			{
				while (current_ != end_ && !predicate_(*current_)) ++current_;
			}
	};

	stream_filter(iterator_type from, iterator_type to, predicate_type predicate):
		from_{from},
		to_{to},
		predicate_{predicate}
	{}

	auto begin() const -> filter_iterator<value_type>
	{
		return filter_iterator<value_type>{from_, to_, predicate_};
	}

	auto end() const -> filter_iterator<value_type>
	{
		return filter_iterator<value_type>{to_, to_, predicate_};
	}

	private:
		iterator_type from_;
		iterator_type to_;
		predicate_type predicate_;
};

template <typename type>
struct filter
{
	using predicate_type = std::function< auto (type const &) -> bool >;

	explicit filter(predicate_type predicate): predicate_{predicate} {}

	predicate_type predicate_;
};

template <typename iterable>
auto operator >>
(
	iterable const &itr,
	filter
	<
		typename std::remove_reference
		<
			decltype(*static_cast<iterable *>(nullptr)->begin())
		>::type
	> const &flt
) -> stream_filter
<
	decltype(static_cast<iterable *>(nullptr)->begin())
>
{
	return stream_filter{itr.begin(), itr.end(), flt.predicate_};
}

}
