#pragma once

#include <cstddef>
#include <vector>
#include <functional>
#include <iterator>

namespace h119::util::stream
{

template <typename value_type>
struct stream_generator
{
	using generator_type = std::function< auto (std::size_t) -> value_type >;

	struct generator_iterator
	{
		generator_iterator(generator_type generator):
			number_{0},
			generator_{generator}
		{}

		auto operator *() -> value_type
		{
			return generator_(number_);
		}

		auto operator ++() -> generator_iterator &
		{
			++number_;
			return *this;
		}

		/*
		 * This runs infinitely!!! Should be used with merge or first_n to have a limit.
		 */
		auto operator !=(generator_iterator const &) const -> bool
		{
			return true;
		}


		private:
			std::size_t number_;
			generator_type generator_;
	};

	stream_generator(generator_type generator): generator_{generator} {}

	auto begin() const -> generator_iterator
	{
		return generator_iterator{generator_};
	}

	auto end() const -> generator_iterator
	{
		return generator_iterator{generator_};
	}

	private:
		generator_type generator_;
};

template <typename generator_type>
auto generate(generator_type generator)
-> stream_generator
<
	decltype((*static_cast<generator_type*>(nullptr))(0))
>
{
	return stream_generator<decltype((*static_cast<generator_type*>(nullptr))(0))>{generator};
}

}
