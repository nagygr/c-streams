#pragma once

#include <functional>
#include <iterator>
#include <utility>

namespace h119::util::stream
{

template <typename input_iterator_type, typename output_value_type>
struct stream_reducer
{
	using input_value_type =
		typename std::remove_reference
		<
			decltype(**static_cast<input_iterator_type *>(nullptr))
		>::type;

	/*
	 * function(intermediate_result, current_value) -> new_intermediate_result
	 */
	using reducer_type =
		std::function
		<
			auto ( output_value_type const &, input_value_type const & ) -> output_value_type
		>;

	stream_reducer
	(
		input_iterator_type from,
		input_iterator_type to,
		output_value_type initial_value,
		reducer_type reducer
	):
		value_{initial_value}
	{
		for (; from != to; ++from)
			value_ = reducer(value_, *from);
	}

	auto value() const -> output_value_type const &
	{
		return value_;
	}

	private:
		output_value_type value_;
};

template <typename input_value_type, typename output_value_type>
struct reduce
{
	using reducer_type =
		std::function
		< 
			auto
			(
				output_value_type const &,
				input_value_type const &
			) -> output_value_type
		>;

	reduce
	(
		output_value_type const &initial_value,
		reducer_type reducer
	):
		initial_value_{initial_value},
		reducer_{reducer}
	{}

	auto initial_value() const -> output_value_type
	{
		return initial_value_;
	}

	auto reducer() const -> reducer_type
	{
		return reducer_;
	}

	private:
		output_value_type initial_value_;
		reducer_type reducer_;
};

template <typename iterable_source, typename output_value_type>
auto operator >>
(
	iterable_source const &itr,
	reduce
	<
		typename std::remove_reference
		<
			decltype(*static_cast<iterable_source*>(nullptr)->begin())
		>::type,
		output_value_type
	> reducer
) -> output_value_type
{
	return stream_reducer
	<
		decltype(static_cast<iterable_source*>(nullptr)->begin()),
		output_value_type
	>
	{
		itr.begin(), itr.end(), reducer.initial_value(), reducer.reducer()
	}.value();
}

}

