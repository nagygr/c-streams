#pragma once

#include <cstddef>
#include <vector>
#include <functional>
#include <iterator>

namespace h119::util::stream
{

template <typename iterator_type>
struct stream_first_n
{
	using value_type = decltype(**static_cast<iterator_type *>(nullptr));

	template <typename value_type>
	struct first_n_iterator
	{
		first_n_iterator(std::size_t max_number, iterator_type from, iterator_type end):
			number_{0},
			max_number_{max_number},
			current_{from},
			end_{end}
		{}

		auto operator *() -> value_type
		{
			return *current_;
		}

		auto operator ++() -> first_n_iterator<value_type> &
		{
			++current_;
			++number_;
			return *this;
		}

		auto operator !=(first_n_iterator<value_type> const &other) const -> bool
		{
			return current_ != other.current_ && number_ != max_number_;
		}

		private:
			std::size_t number_;
			std::size_t max_number_;
			iterator_type current_;
			iterator_type end_;
	};

	stream_first_n(std::size_t max_number, iterator_type from, iterator_type to):
		max_number_{max_number},
		from_{from},
		to_{to}
	{}

	auto begin() const -> first_n_iterator<value_type>
	{
		return first_n_iterator<value_type>{max_number_, from_, to_};
	}

	auto end() const -> first_n_iterator<value_type>
	{
		return first_n_iterator<value_type>{max_number_, to_, to_};
	}

	private:
		std::size_t max_number_;
		iterator_type from_;
		iterator_type to_;
};

struct first_n
{
	first_n(std::size_t number): number_{number} {}

	auto number() const -> std::size_t {return number_;}

	private:
		std::size_t number_;
};

template <typename iterable>
auto operator >> (iterable const &itr, first_n const &fn)
-> stream_first_n
<
	decltype(static_cast<iterable *>(nullptr)->begin())
>
{
	return stream_first_n{fn.number(), itr.begin(), itr.end()};
}

}
