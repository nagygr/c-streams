#pragma once

#include <vector>
#include <functional>
#include <iterator>
#include <type_traits>

namespace h119::util::stream
{

template <typename iterator_type, typename to_type>
struct stream_map
{
	using mapped_type = to_type;
	using value_type =
		typename std::remove_reference
		<
			decltype(**static_cast<iterator_type *>(nullptr))
		>::type;
	using transformation_type = std::function<auto ( value_type const & ) -> to_type>;

	struct map_iterator
	{
		map_iterator(iterator_type current, transformation_type transformation):
			current_{current},
			transformation_{transformation}
		{
		}

		auto operator *() -> to_type
		{
			return transformation_(*current_);
		}

		auto operator ++() -> map_iterator &
		{
			++current_;
			return *this;
		}

		auto operator !=(map_iterator const &other) const -> bool
		{
			return current_ != other.current_;
		}


		private:
			iterator_type current_;
			transformation_type transformation_;
	};

	stream_map(iterator_type from, iterator_type to, transformation_type transformation):
		from_{from},
		to_{to},
		transformation_{transformation}
	{}

	auto begin() const -> map_iterator
	{
		return map_iterator{from_, transformation_};
	}

	auto end() const -> map_iterator
	{
		return map_iterator{to_, transformation_};
	}

	private:
		iterator_type from_;
		iterator_type to_;
		transformation_type transformation_;
};

template <typename from_type, typename to_type>
struct map
{
	using transformation_type = std::function< auto (from_type const &) -> to_type >;

	explicit map(transformation_type transformation): transformation_{transformation} {}

	transformation_type transformation_;
};

template <typename iterable, typename to_type>
auto operator >>
(
	iterable const &itr,
	map
	<
		typename std::remove_reference
		<
			decltype(*static_cast<iterable *>(nullptr)->begin())
		>::type,
		to_type
	> const &m
) -> stream_map
<
	typename std::remove_reference
	<
		decltype(static_cast<iterable *>(nullptr)->begin())
	>::type,
	to_type
>
{
	return stream_map{itr.begin(), itr.end(), m.transformation_};
}

}
