#pragma once

#include <vector>
#include <functional>
#include <iterator>
#include <fstream>
#include <ios>
#include <string_view>
#include <memory>
#include <stdexcept>

#include <h119/util/optional/optional_reference.h>

namespace h119::util::stream
{

struct stream_file
{
	static constexpr std::streamsize default_buffer_size = 2048;

	stream_file(std::fstream &file, std::streamsize buffer_size = default_buffer_size):
		file_{file},
		buffer_size_{buffer_size}
	{}

	struct file_iterator
	{
		file_iterator():
			initialized_{true},
			buffer_size_{0},
			current_{nullptr},
			end_of_buffer_{nullptr}
		{}

		file_iterator
		(
			h119::util::optional::optional_reference<std::fstream> file,
			std::streamsize buffer_size
		);

		auto operator *() -> char
		{
			if (!initialized_)
			{
				fill_buffer();
				initialized_ = true;
			}

			if (current_ == nullptr)
				throw std::out_of_range{"File iterator accessed after end of file"};

			return *current_;
		}

		auto operator ++() -> file_iterator &
		{	
			if (current_ != nullptr)
			{
				++current_;

				if (current_ == end_of_buffer_)
					fill_buffer();
			}

			return *this;
		}

		auto operator !=(file_iterator const &other) const -> bool
		{
			return initialized_ != other.initialized_ || current_ != other.current_;
		}

		private:
			mutable bool initialized_;
			h119::util::optional::optional_reference<std::fstream> file_;
			std::shared_ptr<char[]> buffer_;
			std::streamsize buffer_size_;
			char *current_;
			char *end_of_buffer_;

			auto fill_buffer() -> void;
	};

	auto begin() const -> file_iterator
	{
		return file_iterator{file_, buffer_size_};
	}

	auto end() const -> file_iterator
	{
		return file_iterator{};
	}

	private:
		h119::util::optional::optional_reference<std::fstream> file_;
		std::streamsize buffer_size_;
};

}

