#pragma once

#include <functional>
#include <iterator>
#include <type_traits>
#include <utility>

namespace h119::util::stream
{

template <typename iterator_type>
struct stream_default
{
	using value_type = 
		typename std::remove_reference
		<
			decltype(**static_cast<iterator_type*>(nullptr))
		>::type;

	template <typename value_type>
	struct default_iterator
	{
		default_iterator(iterator_type from, iterator_type to, value_type const &default_value):
			current_{from},
			end_{to},
			default_value_{default_value}
		{}

		auto operator *() -> value_type
		{
			if (current_ != end_)
			{
				return *current_;
			}
			else
			{
				return default_value_;
			}
		}

		auto operator ++() -> default_iterator &
		{
			if (current_ != end_)
				++current_;
			return *this;
		}

		auto operator !=(default_iterator const &) const -> bool
		{
			// This iterator will iterate for ever!!!
			// It is meant to be used together with a merge iterator where the *other* stream is longer
			return true;
		}

		private:
			iterator_type current_;
			iterator_type end_;
			value_type default_value_;
	};

	stream_default(iterator_type from, iterator_type to, value_type const &default_value):
		from_{from},
		to_{to},
		default_value_{default_value}
	{}

	auto begin() const -> default_iterator<value_type>
	{
		return default_iterator<value_type>{from_, to_, default_value_};
	}

	auto end() const -> default_iterator<value_type>
	{
		return default_iterator<value_type>{to_, to_, default_value_};
	}

	private:
		iterator_type from_;
		iterator_type to_;
		value_type default_value_;
};

template <typename iterable>
auto operator |
(
	iterable const &itr,
	typename std::remove_reference<decltype(*static_cast<iterable *>(nullptr)->begin())>::type
		const & default_value
) -> stream_default
<
	decltype(static_cast<iterable *>(nullptr)->begin())
>
{
	return stream_default{itr.begin(), itr.end(), default_value};	
}

}
