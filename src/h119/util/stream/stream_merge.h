#pragma once

#include <functional>
#include <iterator>
#include <type_traits>
#include <utility>

namespace h119::util::stream
{

template <typename left_iterator_type, typename right_iterator_type>
struct stream_merge
{
	using left_value_type =
		typename std::remove_reference
		<
			decltype(**static_cast<left_iterator_type *>(nullptr))
		>::type;

	using right_value_type =
		typename std::remove_reference
		<
			decltype(**static_cast<right_iterator_type *>(nullptr))
		>::type;

	template <typename left_value_type, typename right_value_type>
	struct merge_iterator
	{
		merge_iterator
		(
			left_iterator_type left_current,
			left_iterator_type left_current_end,
			right_iterator_type right_current,
			right_iterator_type right_current_end
		):
			left_current_{left_current},
			left_current_end_{left_current_end},
			right_current_{right_current},
			right_current_end_{right_current_end}
		{}

		auto operator *() -> std::pair<left_value_type, right_value_type>
		{
			return std::make_pair(*left_current_, *right_current_);
		}

		auto operator ++() -> merge_iterator<left_value_type, right_value_type> &
		{
			if (left_current_ != left_current_end_)	++left_current_;
			if (right_current_ != right_current_end_) ++right_current_;
			return *this;
		}

		auto operator !=(merge_iterator const &other) const -> bool
		{
			return 
				left_current_ != other.left_current_
				&& right_current_ != other.right_current_;
		}

		private:
			left_iterator_type left_current_;
			left_iterator_type left_current_end_;
			right_iterator_type right_current_;
			right_iterator_type right_current_end_;
	};

	stream_merge
	(
		left_iterator_type left_from, left_iterator_type left_to,
		right_iterator_type right_from, right_iterator_type right_to
	):
		left_from_{left_from},
		left_to_{left_to},
		right_from_{right_from},
		right_to_{right_to}
	{}

	auto begin() const -> merge_iterator<left_value_type, right_value_type>
	{
		return merge_iterator<left_value_type, right_value_type>{left_from_, left_to_, right_from_, right_to_};
	}

	auto end() const -> merge_iterator<left_value_type, right_value_type>
	{
		return merge_iterator<left_value_type, right_value_type>{left_to_, left_to_, right_to_, right_to_};
	}

	private:
		left_iterator_type left_from_;
		left_iterator_type left_to_;
		right_iterator_type right_from_;
		right_iterator_type right_to_;

};

template <typename left_iterable, typename right_iterable>
auto operator *
(
	left_iterable const &litr,
	right_iterable const &ritr
) -> stream_merge
<
	decltype(static_cast<left_iterable*>(nullptr)->begin()),
	decltype(static_cast<right_iterable*>(nullptr)->begin())
>
{
	return stream_merge{litr.begin(), litr.end(), ritr.begin(), ritr.end()};
}

}
