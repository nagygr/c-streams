#pragma once

#include <h119/util/optional/uninitialized_reference.h>

namespace h119::util::optional
{

template <typename type>
struct optional_reference
{
	inline optional_reference();
	inline optional_reference(type &value);

	inline auto has_value() const -> bool;
	inline auto reset() -> void;

	inline auto value() -> type &;
	inline auto value() const -> type const &;

	inline auto mutable_value() const -> type &;

	inline auto value(type &value) -> optional_reference &;

	inline operator type &() const;

	inline auto operator *() const -> type &;
	inline auto operator ->() const -> type *;
	inline auto operator=(type &value) -> optional_reference &;

	private:
		type *value_;

};

/******************
 * Inline methods *----------------------------------------------------------------------------------
******************/

template <typename type>
inline optional_reference<type>::optional_reference(): value_(nullptr) {}

template <typename type>
inline optional_reference<type>::optional_reference(type &value): value_(&value) {}

template <typename type>
inline auto optional_reference<type>::has_value() const -> bool
{
	return value_ != nullptr;
}

template <typename type>
inline auto optional_reference<type>::value() -> type &
{
	if (value_ == nullptr) throw uninitialized_reference{};
	return *value_;
}

template <typename type>
inline auto optional_reference<type>::value() const -> type const &
{
	if (value_ == nullptr) throw uninitialized_reference{};
	return *value_;
}

/*
 * It makes sense to change an object seen through a reference in a const environment,
 * i.e. if the object that sees the other one through the reference doesn't change
 * in the process then it *is* a const method call on it.
 */
template <typename type>
inline auto optional_reference<type>::mutable_value() const -> type &
{
	return *(const_cast<optional_reference *>(this)->value_);
}

template <typename type>
inline auto optional_reference<type>::value(type &value) -> optional_reference<type> &
{
	value_ = &value;
	return *this;
}

template <typename type>
inline optional_reference<type>::operator type &() const
{
	return *value_;
}

template <typename type>
inline auto optional_reference<type>::reset() -> void
{
	value_ = nullptr;
}

template <typename type>
inline auto optional_reference<type>::operator *() const -> type &
{
	return *value_;
}

template <typename type>
inline auto optional_reference<type>::operator ->() const -> type *
{
	return value_;
}

template <typename type>
inline auto optional_reference<type>::operator=(type &value) -> optional_reference<type> &
{
	value_ = &value;
	return *this;
}
}
