#pragma once

#include <stdexcept>

namespace h119::util::optional
{

struct uninitialized_reference: std::runtime_error
{
	inline uninitialized_reference();
};

/******************
 * Inline methods *----------------------------------------------------------------------------------
******************/

inline uninitialized_reference::uninitialized_reference():
	std::runtime_error{"An unininitalized optional reference was dereferenced"}
{
}

}
